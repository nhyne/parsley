# API Documentation

### GET /api/people/:id
* Returns a JSON representation of the given person if they are found, 404 if they do not exist.

### GET /api/people
* Optional params are:
1. page <INT> which page to view (10 per page)
2. limit <INT> limit to return
3. QUERY PARAMS: first_name, last_name, email, address_street, address_city, address_state, address_zip, phone, terms_accepted, age
* These query params are AND'd together to be more specific
4. Results can be ordered with the order and direction params
* order is the column to be odered by
* direction is either ASC or DESC, defaulting to ASC

### POST /api/people
* Request body:
first_name: STRING,
middle_name: STRING (optional),
last_name: STRING
email: STRING,
dob: DATE,
gender: STRING,
status: STRING,
terms_accepted_at: DATE,
address_street: STRING,
address_city: STRING,
address_state: STRING,
address_zip: STRING,
phone: STRING

### DELETE /api/people/:id
* Destroys the person if they exist, returns 202 if successful. 404 if they cannot be found.

### PUT /api/people/:id
* Updates attributes on an already created person
* Request body:
person: {
  ATTRIBUTES: VALUES
}
* Any attributes can be provided

## Running with Docker Compose

1. Run docker-compose up --build -d
2. You should be up and running at localhost:3000
* There is a SEED variable in .node.env to seed the DB the first time you run compose up. 

## Running Tests

You can either have docker-compose up running, or you can have postgres running on your local.

Unfortunately the test DB has to be seeded in order to cover the pagination test.

* npm install
* NODE_ENV=test node_modules/.bin/sequelize db:create
* NODE_ENV=test node_modules/.bin/sequelize db:migrate
* NODE_ENV=test node_modules/.bin/sequelize db:seed:all
* npm test
