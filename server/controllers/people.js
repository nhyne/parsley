const Person = require('../models').Person;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const PersonFactory = function(req) {
  let today = new Date();
  let birth = new Date(req.body.dob);
  let age = today.getFullYear() - birth.getFullYear();
  let months = today.getMonth() - birth.getMonth();
  if (months < 0 || (months === 0 && today.getDate() < birth.getDate())) {
    age--;
  }

  let terms_accepted = (typeof req.body.terms_accepted_at !== "undefined");

  let id = Math.random().toString(36).substring(6);

  return Person.create({
    id: id,
    first_name: req.body.first_name,
    middle_name: req.body.middle_name,
    last_name: req.body.last_name,
    email: req.body.email,
    dob: req.body.dob,
    age: age,
    gender: req.body.gender,
    status: req.body.status,
    terms_accepted: terms_accepted,
    terms_accepted_at: req.body.terms_accepted_at,
    address_street: req.body.address_street,
    address_city: req.body.address_city,
    address_state: req.body.address_state,
    address_zip: req.body.address_zip,
    phone: req.body.phone
  });
};

const filterQueryAttributes = function(query) {
  var result = [];
  if (query.first_name) {
    result.push({ first_name: { [Op.like]: `%${query.first_name}%` } });
  }

  if (query.last_name) {
    result.push({ last_name: { [Op.like]: `%${query.last_name}%` } });
  }

  if (query.email) {
    result.push({ email: { [Op.like]: `%${query.email}%` } });
  }

  if (query.address_street) {
    result.push({ address_street: { [Op.like]: `%${query.address_street}%` } });
  }

  if (query.address_city) {
    result.push({ address_city: { [Op.like]: `%${query.address_city}%` } });
  }

  if (query.address_state) {
    result.push({ address_state: { [Op.like]: `%${query.address_state}%` } });
  }

  if (query.address_zip) {
    result.push({ address_zip: { [Op.like]: `%${query.address_zip}%` } });
  }

  if (query.phone) {
    result.push({ phone: { [Op.like]: `%${query.phone}%` } });
  }

  if (query.terms_accepted) {
    result.push({ terms_accepted: query.terms_accepted === 'true' ? true : false });
  }

  if(query.age) {
    result.push({ age: parseInt(query.age, 10) });
  }
  return { [Op.and] : result };
};

const sortQuery = function(query) {
  let result = [];
  let direction = 'ASC'; // standard sorting direction
  let columns = [];

  if (query.order) {
    columns = query.order.split(',');
  }

  if (query.direction) {
    direction = query.direction;
  }

  let length = columns.length;
  for (let i = 0; i < length; i ++) {
    result.push([columns[i], direction]);
  }
  return result;
}

module.exports = {
  create(req, res) {
    return PersonFactory(req)
    .then((person) => res.status(201).send(person))
    .catch((error) => res.status(400).send(error));
  },

  get(req, res) {
    return Person.findById(req.params['id'])
    .then((person) => {
      if (person) {
        res.status(200).send(person);
      } else {
        res.status(404).send('This is not the person you are looking for.');
      }
    })
    .catch((error) => res.status(400).send(error));
  },

  view(req, res) {
    let options = {
      limit: req.query.limit,
      offset: (parseInt(req.query.page, 10) - 1) * 10
    };

    let filter = filterQueryAttributes(req.query);

    if (Object.keys(filter[Op.and]).length != 0) {
      options.where = filter;
    }

    let sort = sortQuery(req.query);
    if (sort.length != 0) {
      options.order = sort;
    }

    return Person.findAll(options)
    .then((persons) => res.status(200).json({ people: persons }))
    .catch((error) => res.status(400).send(error));
  },

  delete(req, res) {
    return Person.destroy({ 
      where: { 
        id: req.params['id'] 
      },
      limit: 1
    })
    .then((destroyedCount) => {
      if (destroyedCount != 0) {
        res.status(202).send('Destroyed successfully');
      } else {
        res.status(404).send('Could not find the person to destroy');
      }
    })
    .catch((error) => res.status(400).send(error))
  },

  update(req, res) {
    req.body.person.id = req.params['id']
    return Person.update(req.body.person, {
      where: {
        id: req.params['id'],
      },
      fields: Object.keys(req.body.person),
      returning: true,
      limit: 1
    })
    .then((results) => {
      if (results[0] == 0) {
        res.status(404).send('Could not find the person to update');
      } else {
        res.status(200).send(results[1]);
      }
    })
    .catch((error) => res.status(400).send(error));
  }
}