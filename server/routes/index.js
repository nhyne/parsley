const peopleController = require('../controllers').people;

module.exports = (app) => {
  app.post('/api/people', peopleController.create);
  app.get('/api/people/:id', peopleController.get);
  app.get('/api/people', peopleController.view);
  app.delete('/api/people/:id', peopleController.delete);
  app.put('/api/people/:id', peopleController.update);
}