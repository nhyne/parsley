//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../app');
let should = chai.should();

chai.use(chaiHttp);
//Our parent block
describe('People', () => {
  describe('/GET People', () => {
    it('it should get the first 10 people', (done) => {
      chai.request(app).get('/api/people').end((err, res) => {
        res.should.have.status(200);
        res.body.people.should.be.a('array');
        res.body.people.length.should.be.eql(10);
        done();
      });
    });

    it('it should get the person with the correct ID', (done) => {
      chai.request(app).get('/api/people/ykpdo5km').end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.first_name.should.be.eql('Albin');
        done();
      });
    });

    it('it should get a 404 when looking for a non-existant person', (done) => {
      chai.request(app).get('/api/people/1').end((err, res) => {
        res.should.have.status(404);
        res.text.should.be.eql('This is not the person you are looking for.');
        done();
      });
    });

    it('it should have id mxvv62yn as the first id on the second page of a paginated list sorted by last name', (done) => {
      chai.request(app).get('/api/people/?order=last_name,first_name&direction=ASC&page=2').end((err, res) => {
        res.should.have.status(200);
        res.body.people.should.be.a('array');
        res.body.people[0].id.should.be.eql('mxvv62yn');
        done();
      });
    });

    it('should return people who are 0 years old', (done) => {
      chai.request(app).get('/api/people/?age=0').end((err, res) => {
        res.should.have.status(200);
        res.body.people.should.be.a('array');
        res.body.people.length.should.be.eql(2);
        done();
      });
    });
  });

  describe('/POST People', () => {
    it('it should create a person successfully', (done) => {
      let person = {
        first_name: 'adam',
        middle_name: 'justin',
        last_name: 'johnson',
        email: 'adamjohnson35@gmail.com',
        dob: '1980-10-10',
        gender: 'male',
        status: 'active',
        terms_accepted_at: null,
        address_street: 'street',
        address_city: 'city',
        address_state: 'state',
        address_zip: '0999-1000',
        phone: '555-555-5555'
      };
      chai.request(app).post('/api/people').send(person).end((err, res) => {
        res.should.have.status(201);
        res.body.should.be.a('object');
        res.body.first_name.should.be.eql('adam');
        done();
      });
    });
  });

  describe('/PUT People', () => {
    it('it fails to update with bad DOB', (done) => {
      let body = {
        person: {
          dob: "BADDATE"
        }
      };
      chai.request(app).put('/api/people/ykpdo5km').send(body).end((err, res) => {
        res.should.have.status(400);
        done();
      });
    });

    it('it successfully updates the email, dob, and age', (done) => {
      let body = {
        person: {
          dob: "1999-10-10",
          age: 100,
          email: "thisisnew@cool.io"
        }
      };
      chai.request(app).put('/api/people/ykpdo5km').send(body).end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('array');
        res.body[0].email.should.be.eql('thisisnew@cool.io');
        done();
      });
    });
  });

  describe('/DELETE People', () => {
    it('it should delete a person', (done) => {
      chai.request(app).delete('/api/people/ykpdo5km').end((err, res) => {
        res.should.have.status(202);
        res.text.should.be.eql('Destroyed successfully');
        done();
      });
    });

    it('it cannot delete the same person twice', (done) => {
      chai.request(app).delete('/api/people/ykpdo5km').end((err, res) => {
        res.should.have.status(404);
        res.text.should.be.eql('Could not find the person to destroy');
        done();
      });
    });
  });
});