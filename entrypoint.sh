#!/bin/bash

if [ "$1" = "npm" ] && [ "$2" = "start" ]; then
  dockerize -wait tcp://postgres:5432 -timeout 1m

  NODE_ENV="${NODE_ENV:-development}"

  echo "\n\033[1;34m==> Running migrations \033[0m\n"
  if NODE_ENV=$NODE_ENV ./node_modules/.bin/sequelize db:migrate; then
    
    echo "\n\033[1;32m==> Migrations complete \033[0m\n"
  else
    # Database doesn't exist, create it
    echo "\n\033[1;34m==> Database does not exist, creating it \033[0m\n"
    NODE_ENV=$NODE_ENV ./node_modules/.bin/sequelize db:create
    echo "\n\033[1;32m==> Database created \033[0m\n"

    echo "\n\033[1;34m==> Running migrations again \033[0m\n"
    NODE_ENV=$NODE_ENV ./node_modules/.bin/sequelize db:migrate
    echo "\n\033[1;32m==> Migrations complete \033[0m\n"
  fi

  # Worst case scenario this gets run multiple times and postgres stops it wihout any issues
  if [ "$SEED" = true ]; then
    echo "\n\033[1;34m==> Seeding database \033[0m\n"
    NODE_ENV=$NODE_ENV ./node_modules/.bin/sequelize db:seed:all
    echo "\n\033[1;32m==> Database seeded \033[0m\n"
  fi

  echo "\n\033[1;32m==> Starting Server \033[0m\n"

fi

exec "$@"