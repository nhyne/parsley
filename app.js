const express = require('express');
const bodyParser = require('body-parser');
const paginate = require('express-paginate');

const app = express();

app.use(bodyParser.json());
app.use(paginate.middleware(10, 50));

require('./server/routes')(app);

app.get('*', (req, res) => res.status(200).send({
  message: 'Welcome to the beginning of nothingness.',
}));

module.exports = app;