FROM node:carbon

WORKDIR /srv/www/

RUN wget -nv 'https://github.com/jwilder/dockerize/releases/download/v0.6.1/dockerize-linux-amd64-v0.6.1.tar.gz' && \
    tar -xzvf dockerize-linux-amd64-v0.6.1.tar.gz && \
    mv dockerize /usr/sbin && \
    rm dockerize-linux-amd64-v0.6.1.tar.gz

COPY package*.json ./

ENV NODE_ENV=production
RUN npm install --only=production

COPY . .

COPY ./server/config/compose.json ./server/config/config.json

ENTRYPOINT ["/bin/sh", "/srv/www/entrypoint.sh"]

CMD ["npm", "start"]